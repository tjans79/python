1994 Atlanta Braves

Batters
--------------
C,Javy Lopez\lopezja01,80,303,277,9,0,13,0,17,61,0.245,5
1B,Fred McGriff*\mcgrifr01,113,478,424,25,1,34,7,50,76,0.318,1
2B,Mark Lemke#\lemkema01,104,394,350,15,0,3,0,38,37,0.294,0
SS,Jeff Blauser\blausje01,96,434,380,21,4,6,1,38,64,0.258,5
3B,Terry Pendleton#\pendlte01,77,324,309,18,3,7,2,12,57,0.252,0
LF,Ryan Klesko*\kleskry01,92,276,245,13,3,17,1,26,48,0.278,1
CF,Roberto Kelly\kellyro01,63,281,255,15,3,6,10,24,36,0.286,0
RF,David Justice*\justida01,104,424,352,16,2,19,2,69,45,0.313,2
CF,Deion Sanders*\sandede02,46,211,191,10,0,4,19,16,28,0.288,1
LF,Dave Gallagher\gallada01,89,177,152,5,0,2,0,22,17,0.224,1
C,Charlie O\'Brien\o\'brich01,51,172,152,11,0,8,0,15,24,0.243,3
OF,Tony Tarasco*\tarasto01,87,144,132,6,0,5,5,9,17,0.273,0
3B,Bill Pecota\pecotbi01,64,130,112,5,0,2,1,16,16,0.214,0
MI,Rafael Belliard\bellira01,46,127,120,7,1,0,0,2,29,0.242,2
OF,Mike Kelly\kellymi02,30,80,77,10,1,2,0,2,17,0.273,1
3B,Jose Oliva\olivajo01,19,66,59,5,0,6,0,7,10,0.288,0
OF,Jarvis Brown\brownja03,17,16,15,1,0,1,0,0,2,0.133,0
SS,Mike Mordecai\mordemi01,4,5,4,0,0,1,0,1,0,0.25,0
P,Greg Maddux\maddugr01,25,73,63,2,0,0,0,1,19,0.222,0
P,Tom Glavine*\glavito02,26,70,56,1,0,0,0,5,15,0.179,0
P,Steve Avery*\averyst01,24,58,49,2,0,0,0,1,16,0.102,0
P,John Smoltz\smoltjo01,21,47,37,1,0,1,0,4,14,0.162,0
P,Kent Mercker*\merckke01,20,43,37,0,0,0,0,2,15,0.054,0
P,Mike Stanton*\stantmi02,49,4,3,0,0,0,0,0,0,0.667,0
P,Mike Bielecki\bielemi01,19,4,3,0,0,0,0,0,3,0.0,0
P,Steve Bedrosian\bedrost01,46,2,2,0,0,0,0,0,1,0.5,0
P,Brad Woodall#\woodabr01,1,2,2,0,0,0,0,0,0,0.5,0
P,Mark Wohlers\wohlema01,51,2,1,0,0,0,0,0,0,1.0,0
P,Greg McMichael\mcmicgr01,51,1,1,0,0,0,0,0,0,0.0,0
P,Gregg Olson\olsongr01,16,1,1,0,0,0,0,0,1,0.0,0
P,Milt Hill\hillmi01,10,0,0,0,0,0,0,0,0,0.0,0

Pitchers
--------------
SP,Greg Maddux\maddugr01,1.56,25,25,202.0,31,156
SP,Tom Glavine*\glavito02,3.97,25,25,165.1,70,140
SP,Steve Avery*\averyst01,4.04,24,24,151.2,55,122
SP,John Smoltz\smoltjo01,4.14,21,21,134.2,48,113
SP,Kent Mercker*\merckke01,3.45,20,17,112.1,45,111
CL,Greg McMichael\mcmicgr01,3.84,51,0,58.2,19,47
RP,Mark Wohlers\wohlema01,4.59,51,0,51.0,33,58
RP,Steve Bedrosian\bedrost01,3.33,46,0,46.0,18,43
RP,Mike Stanton*\stantmi02,3.55,49,0,45.2,26,35
RP,Mike Bielecki\bielemi01,4.0,19,1,27.0,12,18
,Gregg Olson\olsongr01,9.2,16,0,14.2,13,10
,Milt Hill\hillmi01,7.94,10,0,11.1,6,10
,Brad Woodall*\woodabr01,4.5,1,1,6.0,2,2

Service Time
--------------
Steve Avery\averyst01,5
Steve Bedrosian\bedrost01,13
Rafael Belliard\bellira01,13
Mike Bielecki\bielemi01,11
Jeff Blauser\blausje01,8
Jarvis Brown\brownja03,4
Dave Gallagher\gallada01,8
Tom Glavine HOF\glavito02,8
Milt Hill\hillmi01,4
David Justice\justida01,6
Mike Kelly\kellymi02,1st
Roberto Kelly\kellyro01,8
Ryan Klesko\kleskry01,3
Mark Lemke\lemkema01,7
Javy Lopez\lopezja01,3
Greg Maddux HOF\maddugr01,9
Fred McGriff\mcgrifr01,9
Greg McMichael\mcmicgr01,2
Kent Mercker\merckke01,6
Mike Mordecai\mordemi01,1st
Charlie O\'Brien\o\'brich01,9
Jose Oliva\olivajo01,1st
Gregg Olson\olsongr01,7
Bill Pecota\pecotbi01,9
Terry Pendleton\pendlte01,11
Deion Sanders\sandede02,6
John Smoltz HOF\smoltjo01,7
Mike Stanton\stantmi02,6
Tony Tarasco\tarasto01,2
Mark Wohlers\wohlema01,4
Brad Woodall\woodabr01,1st

Fielders
--------------
Steve Avery\averyst01,24,31,.968,
Steve Bedrosian\bedrost01,46,8,.875,
Rafael Belliard\bellira01,44,132,.992,0
Mike Bielecki\bielemi01,19,7,1.000,
Jeff Blauser\blausje01,96,429,.970,11
Jarvis Brown\brownja03,9,10,1.000,-31
Dave Gallagher\gallada01,78,98,.990,7
Tom Glavine\glavito02,25,45,.978,
Milt Hill\hillmi01,10,2,1.000,
David Justice\justida01,102,211,.948,3
Mike Kelly\kellymi02,25,26,.962,-18
Roberto Kelly\kellyro01,63,132,.985,-13
Ryan Klesko\kleskry01,78,99,.929,-6
Mark Lemke\lemkema01,103,512,.994,8
Javy Lopez\lopezja01,75,597,.995,-9
Greg Maddux\maddugr01,25,62,.935,
Fred McGriff\mcgrifr01,112,1077,.994,7
Greg McMichael\mcmicgr01,51,10,.800,
Kent Mercker\merckke01,20,19,1.000,
Mike Mordecai\mordemi01,4,5,1.000,65
Charlie O\'Brien\o\'brich01,48,337,.991,6
Jose Oliva\olivajo01,16,44,.932,10
Gregg Olson\olsongr01,16,0,,
Bill Pecota\pecotbi01,33,79,.975,31
Terry Pendleton\pendlte01,77,220,.950,8
Deion Sanders\sandede02,46,102,.980,15
John Smoltz\smoltjo01,21,29,.966,
Mike Stanton\stantmi02,49,12,1.000,
Tony Tarasco\tarasto01,45,42,1.000,-7
Mark Wohlers\wohlema01,51,12,.917,
Brad Woodall\woodabr01,1,3,1.000,

