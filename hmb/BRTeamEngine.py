# import libraries
import urllib.request
import re
from bs4 import BeautifulSoup
from bs4 import Comment
import operator
import sys
import BRBatter, BRPitcher, BRServiceTime, BRFielder
import os
import pprint
import operator

# TODO: Get current year's roster but last year's stats
# TODO: Command line switches to kill cache
class BRTeamEngine:
    valid_batting_stats = ['player', 'pos', 'AB', 'G', 'PA', 'batting_avg', 'onbase_perc', 'H', '2B', '3B', 'HR', 'SB', 'SO', 'SF', 'HBP', 'BB','award_summary']
    valid_fielding_stats = ['G', 'pos', 'chances', 'PO', 'A', 'E_def', 'Inn_def']
    valid_pitching_stats = ['player', 'IP', 'ER', 'G', 'GS', 'SO', 'BB', 'SV' ]
    regex = re.compile(r"\(.*?\)|\*|\#", re.IGNORECASE)
    soup = None
    debugPlayer = None
    #debugPlayer = '/players/y/yelicch01.shtml'
    year = ""
    team = ""
    city = ""
    fielders = {}
    slugify_strip_re = re.compile(r'[^\w\s-]')
    slugify_hyphenate_re = re.compile(r'[-\s]+')

    def __init__(self, team, year):
        # name regex for removing things like (10-day DL, 40-man roster, etc)
        self.team = team
        self.year = year
        self.team_full_name = ""
        self.city = ""

        # specify the url
        team_url = 'https://www.baseball-reference.com/teams/' + team + '/' + year + '.shtml'

        team_html = self.getOrCached(team_url)

        # parse the html using beautiful soap and store in variable `soup`
        self.soup = BeautifulSoup(team_html, 'html.parser')

        team_name = self.soup.find('h1', attrs={'itemprop': 'name'}).find_all('span')

        self.city = team_name[1].text.rsplit(' ', 1)[0]
        self.team_full_name = '{0} {1}'.format(team_name[0].text.strip(), team_name[1].text.strip())

    def getOrCached(self, url):
        fileName = 'cache/' + self.slugify(url)
        
        if not os.path.exists('cache'):
            os.makedirs('cache')

        try:
            fh = open(fileName, 'r')
            return fh.read()
        except FileNotFoundError:
            # query the website and return the html to the variable 'html'
            httpResponse = urllib.request.urlopen(url)
            responseBody = str(httpResponse.read())
            fh = open(fileName, 'w')
            fh.write(responseBody)
            return responseBody

    def slugify(self, value):
        value = value.replace('/','_') \
            .replace(':','_') \
            .replace('.','_')
        return value

    def getFieldersAdvanced(self):
        url = "https://www.baseball-reference.com/teams/" + self.team + "/" + self.year + "-fielding.shtml"
        page_html = self.getOrCached(url)
        page_soup = BeautifulSoup(page_html, 'html.parser')

        comments = page_soup.findAll(text=lambda text:isinstance(text, Comment))

        # for some reason this table is wrapped in a comment.  Need to find the comment it's in and bust it out
        for comment in comments:
            if "id=\"players_standard_fielding_c\"" in comment:
                comment_soup = BeautifulSoup(comment, 'html.parser')
                catcher_soup = comment_soup.find('table', attrs={'id': 'players_standard_fielding_c'}) \
                .tbody.find_all('tr', class_=lambda x: x != 'thead') 
            elif "id=\"players_standard_fielding_1b\"" in comment:
                comment_soup = BeautifulSoup(comment, 'html.parser')
                first_soup = comment_soup.find('table', attrs={'id': 'players_standard_fielding_1b'}) \
                .tbody.find_all('tr', class_=lambda x: x != 'thead')
            elif "id=\"players_standard_fielding_2b\"" in comment:
                comment_soup = BeautifulSoup(comment, 'html.parser')
                second_soup = comment_soup.find('table', attrs={'id': 'players_standard_fielding_2b'}) \
                .tbody.find_all('tr', class_=lambda x: x != 'thead')
            elif "id=\"players_standard_fielding_3b\"" in comment:
                comment_soup = BeautifulSoup(comment, 'html.parser')
                third_soup = comment_soup.find('table', attrs={'id': 'players_standard_fielding_3b'}) \
                .tbody.find_all('tr', class_=lambda x: x != 'thead')
            elif "id=\"players_standard_fielding_ss\"" in comment:
                comment_soup = BeautifulSoup(comment, 'html.parser')
                short_soup = comment_soup.find('table', attrs={'id': 'players_standard_fielding_ss'}) \
                .tbody.find_all('tr', class_=lambda x: x != 'thead')
            elif "id=\"players_standard_fielding_lf\"" in comment:
                comment_soup = BeautifulSoup(comment, 'html.parser')
                left_soup = comment_soup.find('table', attrs={'id': 'players_standard_fielding_lf'}) \
                .tbody.find_all('tr', class_=lambda x: x != 'thead')
            elif "id=\"players_standard_fielding_rf\"" in comment:
                comment_soup = BeautifulSoup(comment, 'html.parser')
                right_soup = comment_soup.find('table', attrs={'id': 'players_standard_fielding_rf'}) \
                .tbody.find_all('tr', class_=lambda x: x != 'thead')
            elif "id=\"players_standard_fielding_cf\"" in comment:
                comment_soup = BeautifulSoup(comment, 'html.parser')
                center_soup = comment_soup.find('table', attrs={'id': 'players_standard_fielding_cf'}) \
                .tbody.find_all('tr', class_=lambda x: x != 'thead')
            elif "id=\"players_standard_fielding_p\"" in comment:
                comment_soup = BeautifulSoup(comment, 'html.parser')
                pitcher_soup = comment_soup.find('table', attrs={'id': 'players_standard_fielding_p'}) \
                .tbody.find_all('tr', class_=lambda x: x != 'thead')
            elif "id=\"players_DH_games\"" in comment:
                comment_soup = BeautifulSoup(comment, 'html.parser')
                dh_soup = comment_soup.find('table', attrs={'id': 'players_DH_games'}) \
                .tbody.find_all('tr', class_=lambda x: x != 'thead')

        self.getFieldersForPosition(catcher_soup, 'C')
        self.getFieldersForPosition(first_soup, '1B')
        self.getFieldersForPosition(second_soup, '2B')
        self.getFieldersForPosition(third_soup, '3B')
        self.getFieldersForPosition(short_soup, 'SS')
        self.getFieldersForPosition(left_soup, 'LF')
        self.getFieldersForPosition(right_soup, 'RF')
        self.getFieldersForPosition(center_soup, 'CF')
        self.getFieldersForPosition(pitcher_soup, 'P')
        self.getFieldersForPosition(dh_soup, 'DH')

        # # Display Hernan Perez data for testing
        # # Now need to figure out E (F%), T (Ast), and CD (RTOTyr)
        # # modify url to be dynamic
        # # spit out the values into notepad
        # player = self.fielders['perezhe01']
        # positions = player['positions']
        # appearances = player['appearances']

        # # Position data on the player is unsorted, so sort the positions played by number of games most to least
        # appearances = sorted(appearances.items(), key=operator.itemgetter(1), reverse=True)

        # # loop through the sorted appearances and do stuff with the data for that position
        # for posValues in appearances:
        #     pos,games = posValues
        #     print(pos)
        #     pprint.pprint(positions[pos])

        return self.fielders

    def getFieldersForPosition(self, field_soup, position):
        for row in field_soup:
            data = {}
            cells = row.find_all(['td', 'th'])

            if cells[0].text != 'League Average':
                for cell in cells:
                    if cell['data-stat'] == 'player': 
                        name = cell.text.strip()
                        name_parts = name.split('\xa0', 1) #re.sub(self.regex, "", name).split(' ', 1) 
                        playerUrl = cell.find('a')
                        if playerUrl != None:
                            href = playerUrl['href']
                            slashIndex = href.rindex("/") + 1
                            dotIndex = href.rindex(".")
                            playerId = href[slashIndex:dotIndex]
                    else:
                        data[cell['data-stat']] = cell.text
            
                # create a new player if this is the first time this player was found
                if not playerId in self.fielders:
                    self.fielders[playerId] = {'first_name':name_parts[0], 'last_name':name_parts[1]}
                
                # get the player
                player = self.fielders[playerId]
                if not 'appearances' in player:
                    player['appearances'] = {}

                if not 'positions' in player:
                    player['positions'] = {}

                # these all seem to happen by reference
                positions = player['positions']
                appearances = player['appearances']

                if position == 'DH':
                    appearances[position] = int(data['GS'])
                    positions[position] = {
                        'G': data['G'],
                        'A': 0,
                        'rtot_yr': 0,
                        'fld':0
                    }
                else:
                    appearances[position] = int(data['G'])

                    total_zone = 0
                    if 'tz_runs_total_per_season' in data:
                        total_zone = data['tz_runs_total_per_season']

                    # Position, G, FLD%, Ast, RTotYr          
                    positions[position] = {
                        'G': data['G'],
                        'A': data['A'],
                        'rtot_yr': total_zone,
                        'fld':data['fielding_perc']
                    }

    def getBatters(self):
        batters = []

        # now get the batters
        team_roster_soup = self.soup.find('table', attrs={'id': 'team_batting'}) \
        .tbody.find_all('tr', class_=lambda x: x != 'thead')
        
        # this starts on the team page
        for batter_roster_row in team_roster_soup:
            roster_batter_cells = batter_roster_row.find_all('td')

            position = ""
            roster_batter_info = {} #dictionary of batter info, mostly position and href
            for cell in roster_batter_cells:
                #print(cell['data-stat'])
                if cell['data-stat'] == 'player': 
                    name = cell.text.strip()
                    name_parts = name.split(' ', 1) #re.sub(self.regex, "", name).split(' ', 1) 
                    playerUrl = cell.find('a')['href'] 
                    slashIndex = playerUrl.rindex("/") + 1
                    dotIndex = playerUrl.rindex(".")
                    playerId = playerUrl[slashIndex:dotIndex]
                else:
                    roster_batter_info[cell['data-stat']] = cell.text
         
            # end foreach cell in the row

            position = roster_batter_info['pos']
        
            processBatter = True #replace with player name at some point for debugging
            
            if processBatter:
                ##########################################
                #	Final output
                ##########################################
                # 			
                # G	PA	AB	2B	3B	HR	SB	BB	SO	BA	HBP
                batter = BRBatter.BRBatter()
                batter.FirstName = name_parts[0].strip()
                batter.LastName = name_parts[1].strip()
                batter.Position = position
                batter.PlayerId = playerId
                batter.G = int(roster_batter_info['G'])
                batter.PA = int(roster_batter_info['PA'])
                batter.AB = int(roster_batter_info['AB'])
                batter.Doubles = int(roster_batter_info['2B'])
                batter.Triples = int(roster_batter_info['3B'])
                batter.HR = int(roster_batter_info['HR'])
                batter.SB = int(roster_batter_info['SB'])
                batter.BB = int(roster_batter_info['BB'])
                batter.SO = int(roster_batter_info['SO'])
                if roster_batter_info['batting_avg'] != "":
                    batter.BA = float(roster_batter_info['batting_avg'])
                else:
                    batter.BA = .000  

                batter.HBP = int(roster_batter_info['HBP'])
                
                batter.Year = self.year
                batter.City = self.city
                batters.append(batter)
            else:
                pass # print(self.debugPlayer + "!=" + roster_batter_info['href'])
        #end for batter_roster_row in team_roster_soup:
        return batters    
    # end getBatters

    def getPitchers(self):
        pitchers = []
       
        team_roster_soup = self.soup.find('table', attrs={'id': 'team_pitching'}) \
        .tbody.find_all('tr', class_=lambda x: x != 'thead')    

        # this starts on the team page
        for pitcher_roster_row in team_roster_soup:
            roster_pitcher_cells = pitcher_roster_row.find_all('td')

            position = ""
            roster_pitcher_info = {} #dictionary of pitcher info, mostly position and href
            for cell in roster_pitcher_cells:
                if cell['data-stat'] == 'player': 
                    name = cell.text.strip()
                    name_parts = name.split(' ', 1) #re.sub(self.regex, "", name).split(' ', 1) 
                    playerUrl = cell.find('a')['href'] 
                    slashIndex = playerUrl.rindex("/") + 1
                    dotIndex = playerUrl.rindex(".")
                    playerId = playerUrl[slashIndex:dotIndex]
                else:
                    roster_pitcher_info[cell['data-stat']] = cell.text
            # end foreach cell in the row

            position = roster_pitcher_info['pos']
        
            processPitcher = True #replace with player name at some point for debugging
            
            if processPitcher:
                ##########################################
                #	Final output
                ##########################################
                # 			
                # Pos, Name, ERA, G, GS, IP, BB, SO
                pitcher = BRPitcher.BRPitcher()
                pitcher.FirstName = name_parts[0].strip()
                pitcher.LastName = name_parts[1].strip()
                pitcher.Position = position
                pitcher.PlayerId = playerId
                pitcher.ERA = float(roster_pitcher_info['earned_run_avg'])    
                pitcher.G = int(roster_pitcher_info['G'])
                pitcher.GS = int(roster_pitcher_info['GS'])
                pitcher.IP = float(roster_pitcher_info['IP'])
                pitcher.BB = int(roster_pitcher_info['BB'])
                pitcher.SO = int(roster_pitcher_info['SO'])
                pitcher.Year = self.year
                pitcher.City = self.city
                pitchers.append(pitcher)
            else:
                pass # print(self.debugPlayer + "!=" + roster_pitcher_info['href'])

        return pitchers   
    # end getPitchers

    def getServiceTime(self):
        serviceTimes = []
        
        service_time_soup = None

        # for some reason this table is wrapped in a comment.  Need to find the comment it's in and bust it out
        for comment in self.soup.findAll(text=lambda text:isinstance(text, Comment)):
            if "id=\"appearances\"" in comment:
                comment_soup = BeautifulSoup(comment, 'html.parser')
                service_time_soup = comment_soup.find('table', attrs={'id': 'appearances'}) \
                .tbody.find_all('tr', class_=lambda x: x != 'thead') 
                break

        if service_time_soup:
            # this starts on the team page
            for servicetime_roster_row in service_time_soup:
                roster_servicetime_cells = servicetime_roster_row.find_all(['td','th'])

                roster_servicetime_info = {} #dictionary of servicetime info, mostly position and href
                for cell in roster_servicetime_cells:
                    if cell['data-stat'] == 'player': 
                        name = cell.text.strip()
                        name_parts = name.split(' ', 1) #re.sub(self.regex, "", name).split(' ', 1) 
                        playerUrl = cell.find('a')['href'] 
                        slashIndex = playerUrl.rindex("/") + 1
                        dotIndex = playerUrl.rindex(".")
                        playerId = playerUrl[slashIndex:dotIndex]
                    else:
                        roster_servicetime_info[cell['data-stat']] = cell.text
                # end foreach cell in the row

                processServiceTime = True

                if processServiceTime:

                    ##########################################
                    #	Final output
                    ##########################################
                    # 			
                    serviceTime = BRServiceTime.BRServiceTime()
                    serviceTime.FirstName = name_parts[0].strip()
                    serviceTime.LastName = name_parts[1].strip()
                    serviceTime.PlayerId = playerId
                    serviceTime.Years = roster_servicetime_info['experience']
                    serviceTimes.append(serviceTime)
                else:
                    pass

        return serviceTimes   
    # end getServiceTime

    def getFielders(self):
        fielders = []
        
        fielder_soup = None

        # for some reason this table is wrapped in a comment.  Need to find the comment it's in and bust it out
        for comment in self.soup.findAll(text=lambda text:isinstance(text, Comment)):
            if "id=\"standard_fielding\"" in comment:
                comment_soup = BeautifulSoup(comment, 'html.parser')
                fielder_soup = comment_soup.find('table', attrs={'id': 'standard_fielding'}) \
                .tbody.find_all('tr', class_=lambda x: x != 'thead') 
                break

        if fielder_soup:
            # this starts on the team page
            for fielding_roster_row in fielder_soup:
                roster_fielding_cells = fielding_roster_row.find_all(['td','th'])

                roster_fielding_info = {} #dictionary of servicetime info, mostly position and href
                for cell in roster_fielding_cells:
                    if cell['data-stat'] == 'player': 
                        name = cell.text.strip()
                        name_parts = name.split(' ', 1) #re.sub(self.regex, "", name).split(' ', 1) 
                        playerUrl = cell.find('a')['href'] 
                        slashIndex = playerUrl.rindex("/") + 1
                        dotIndex = playerUrl.rindex(".")
                        playerId = playerUrl[slashIndex:dotIndex]
                    else:
                        roster_fielding_info[cell['data-stat']] = cell.text
                # end foreach cell in the row

                processFielding = True

                if processFielding:

                    ##########################################
                    #	Final output
                    ##########################################
                    # 			
                    fielder = BRFielder.BRFielder()
                    fielder.FirstName = name_parts[0].strip()
                    fielder.LastName = name_parts[1].strip()
                    fielder.PlayerId = playerId
                    fielder.G = roster_fielding_info['G']
                    fielder.CH = roster_fielding_info['chances']
                    fielder.FLP = roster_fielding_info['fielding_perc']
                    fielder.RTotYr = roster_fielding_info['tz_runs_total_per_season']
                    fielders.append(fielder)
                else:
                    pass

        return fielders   
    # end getServiceTime