

class BRPitcher:
    FirstName = ""
    LastName = ""
    G = 0
    GS = 0
    ERA = 0.00
    IP  = 0.00
    BB = 0
    SO = 0
    Position = ""
    PlayerId = ""
    Years = 0
    RtotYr = 0

    def getHmbStatLine(self):
     return (
         f"{self.Position},{self.FirstName} {self.LastName}\\{self.PlayerId},{self.ERA},{self.G},{self.GS},{self.IP},{self.BB},{self.SO}"
     )