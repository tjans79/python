# import libraries
import FGEngine
import sys
import spinner
import subprocess
import HMBRater
from pprint import pprint
import json
import os
import BRServiceTime, BRFielder

#clear = lambda: subprocess.call('cls||clear', shell=True)
#clear()

# get team and year arguments
if len(sys.argv) == 3:
	year_arg = sys.argv[1] #"2017"
	team_arg = sys.argv[2]
else:
	year_arg = '1994'
	team_arg = 'ATL'

# https://www.baseball-reference.com/leagues/NL/2017.shtml

# make it so you can potentially pass a list of teams and player names or find a way to let them choose the player
# perhaps even better, store it in sqlite and query it from there, adding players to a shopping cart.
# also need a way to bust cache if you want an in-season team

fge = FGEngine.FGTeamEngine(team_arg, year_arg)

batters = fge.getBatters()
service_times = []
fielders = []

#clear the file
f = open("output.txt", "w")

f.write(fge.team_full_name + "\n\n")
f.write("Batters\n")
f.write("--------------\n")
for batter in batters:
	f.write(batter.getHmbStatLine())
	f.write("\n")

	serviceTime = BRServiceTime.BRServiceTime()
	serviceTime.FirstName = batter.FirstName
	serviceTime.LastName = batter.LastName
	serviceTime.PlayerId = batter.PlayerId
	serviceTime.Years = batter.Years
	service_times.append(serviceTime)

	fielder = BRFielder.BRFielder()
	fielder.FirstName = batter.FirstName
	fielder.LastName = batter.LastName
	fielder.PlayerId = batter.PlayerId
	fielder.G = batter.G
	fielder.CH = 100
	fielder.FLP = .999
	fielder.RTotYr = batter.RtotYr
	fielders.append(fielder)

f.write("\n")

pitchers = fge.getPitchers()
f.write("Pitchers\n")
f.write("--------------\n")
for pitcher in pitchers:
	f.write(pitcher.getHmbStatLine())
	f.write("\n")

	serviceTime = BRServiceTime.BRServiceTime()
	serviceTime.FirstName = pitcher.FirstName
	serviceTime.LastName = pitcher.LastName
	serviceTime.PlayerId = pitcher.PlayerId
	serviceTime.Years = pitcher.Years
	service_times.append(serviceTime)

	fielder = BRFielder.BRFielder()
	fielder.FirstName = pitcher.FirstName
	fielder.LastName = pitcher.LastName
	fielder.PlayerId = pitcher.PlayerId
	fielder.G = pitcher.G
	fielder.CH = 0
	fielder.FLP = .999
	fielder.RTotYr = 7
	fielders.append(fielder)

f.write("\n")

f.write("Service Time\n")
f.write("--------------\n")

for serviceTime in service_times:
	f.write(serviceTime.getHmbStatLine())
	f.write("\n")

f.write("\n")

f.write("Fielding\n")
f.write("--------------\n")

for fielder in fielders:
	f.write(fielder.getHmbStatLine())
	f.write("\n")

f.write("\n")

f.write("\n")
f.close()

outputPath = os.path.abspath("output.txt")
subprocess.call(['cmd.exe', '/c', outputPath])