# import libraries
import BRTeamEngine
import StatisProBaseball
import sys
import spinner
import subprocess
import HMBRater
from pprint import pprint
import json
import os
import pprint
import operator

clear = lambda: subprocess.call('cls||clear', shell=True)
clear()

# get team and year arguments
if len(sys.argv) == 3:
	year_arg = sys.argv[1] #"2017"
	team_arg = sys.argv[2]
else:
	year_arg = '2019'
	team_arg = 'BOS'

# https://www.baseball-reference.com/leagues/NL/2017.shtml

# make it so you can potentially pass a list of teams and player names or find a way to let them choose the player
# perhaps even better, store it in sqlite and query it from there, adding players to a shopping cart.
# also need a way to bust cache if you want an in-season team

bre = BRTeamEngine.BRTeamEngine(team_arg, year_arg)
spb = StatisProBaseball.StatisProBaseball()

fielders = bre.getFieldersAdvanced()

for playerId, player in fielders.items():
	print(player['first_name'] + ' ' + player['last_name'])
	ratings = spb.RateFielding(player)
	print(ratings)

#batters = bre.getBatters()

# #clear the file
#f = open("output.txt", "w")
#f.write(bre.team_full_name + "\n\n")

#f.write("Advanced Fielding\n")
#f.write("--------------\n")


# f.write("Batters\n")
# f.write("--------------\n")
# for batter in batters:
# 	f.write(batter.getHmbStatLine())
# 	f.write("\n")

# f.write("\n")

# f.write("Pitchers")
# f.write("\n--------------\n")
# pitchers = bre.getPitchers()
# for pitcher in pitchers:
# 	f.write(pitcher.getHmbStatLine())
# 	f.write("\n")

# f.write("\n")

# f.write("Service Time\n")
# f.write("--------------\n")
# serviceTimes = bre.getServiceTime()
# for serviceTime in serviceTimes:
# 	f.write(serviceTime.getHmbStatLine())
# 	f.write("\n")

# f.write("\n")

# f.write("Fielders\n")
# f.write("--------------\n")
# fielders = bre.getFielders()
# for fielder in fielders:
# 	f.write(fielder.getHmbStatLine())
# 	f.write("\n")

#f.write("\n")
#f.close()

#outputPath = os.path.abspath("output.txt")
#subprocess.call(['cmd.exe', '/c', outputPath])