import BRBatter
import HMBBatter

'''
TODO: Test base-running (Yelich had a "no quality")
'''
class HMBRater:
    def __init__(self):
        pass

    def processBatter(self, source: BRBatter.BRBatter):
        batter = HMBBatter.HMBBatter()
        batter.FirstName = source.FirstName
        batter.LastName = source.LastName
        batter.Bats = "S" if source.Bats == "B" else source.Bats
        batter.City = source.City
        batter.Year = source.Year
        batter.Usage = self.getUsage(source)
        batter.Running = self.getRunning(source)
        experienceResult = self.getExperience(source)
        
        experience,avgAdjustment,eraAdjustment = experienceResult
        batter.Experience = experience
        
        #fielding, average, walks, strikeouts, power
        return batter 

    def getExperience(self, source: BRBatter.BRBatter):
        '''
        These are the affects that experience has on AVG/ERA

        Batting average:
        Icon increases BA by .015
        Semi-Icon increases BA by .0075
        Semi-Prospect lowers BA by .00375
        Prospect lowers BA by .0075

        ERA:
        Icon  lowers ERA by .075
        Semi-Icon lowers ERA by .0375
        Semi-Prospect adds .075 to ERA
        Prospect adds .15 to ERA 
        '''
        PROSPECT = "PROSPECT"
        _PROSPECT = "&#149;PROSPECT"
        ICON = "ICON"
        _ICON = "&#149;ICON"

        avgAdjustment = 0
        eraAdjustment = 0

        if(source.Seasons <= 1): 
            quality = PROSPECT 
            avgAdjustment = .0075
            eraAdjustment = -.15
        elif(source.Seasons <= 2):  
            quality = _PROSPECT 
            avgAdjustment = .00375
            eraAdjustment = -.075
        elif(source.Seasons <= 5):
            quality = ""	
        elif(source.Seasons <= 8): 
            quality = _ICON
            avgAdjustment = -.0075
            eraAdjustment = .0375
        else:
            avgAdjustment = -.015
            eraAdjustment = .075
            quality = ICON

        return [quality, avgAdjustment, eraAdjustment]
		#prospect +.0075, _prospect +.00375, _icon -.0075, icon -.015


    def getRunning(self, source:BRBatter.BRBatter):
        DBACTIVE = "DB ACTIVE"
        ACTIVE = "ACTIVE"
        _ACTIVE = "&#149;ACTIVE"
        STOIC = "STOIC"
        _STOIC = "&#149;STOIC"

        SAB = source.SB/source.AB if source.AB > 0 else 0
        
        if(SAB >= .16): return DBACTIVE
        elif(SAB >= .09): return ACTIVE
        elif(SAB >= .04): return _ACTIVE
        elif(SAB >= .01): return ""
        elif(SAB >= .0025): return _STOIC
        else: return STOIC

    def getUsage(self, source: BRBatter.BRBatter):
        HIGH = "H"
        BENCH = "I"
        quality = ""

        if(source.Games > 95):
            quality = HIGH
        elif(source.Games >=50 and source.Games <= 94):
            quality = BENCH

        return quality

