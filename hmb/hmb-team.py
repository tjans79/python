# import libraries
import urllib2
import re
from bs4 import BeautifulSoup
from bs4 import Comment
import operator
import sys

debugPlayer = None
debugPlayer = None # '/players/g/grandya01.shtml'

# get team and year arguments
if len(sys.argv) == 3:
	year_arg = sys.argv[1] #"2017"
	team_arg = sys.argv[2]
else:
	year_arg = '2017'
	team_arg = 'LAD'

# https://www.baseball-reference.com/leagues/NL/2017.shtml

def mk_int(s):
    s = s.strip()
    return int(s) if s else 0

# uses 2016 fielding cutoffs as a baseline
def getFielding(position, FP, chances):
	if chances < 100 or position == 'DH': return "NONE"

	if position == '1B':
		if FP >= .9983:
			return 'GOLD'
		elif FP >= .9967:
			return '&#149;GOLD'
		elif FP >= .9907:
			return 'NONE'
		elif FP >= .9863:
			return '&#149;IRON'
		else:
			return 'IRON'

	elif position == '2B':
		if FP >= .9950:
			return 'GOLD'
		elif FP >= .9915:
			return '&#149;GOLD'
		elif FP >= .9767:
			return 'NONE'
		elif FP >= .9748:
			return '&#149;IRON'
		else:
			return 'IRON'


	elif position == '3B':
		if FP >= .9785:
			return 'GOLD'
		elif FP >= .9754:
			return '&#149;GOLD'
		elif FP >= .9392:
			return 'NONE'
		elif FP >= .9328:
			return '&#149;IRON'
		else:
			return 'IRON'


	elif position == 'C':
		if FP >= .9984:
			return 'GOLD'
		elif FP >= .9968:
			return '&#149;GOLD'
		elif FP >= .9904:
			return 'NONE'
		elif FP >= .9879:
			return '&#149;IRON'
		else:
			return 'IRON'


	elif position in ['OF','LF','RF','CF']:
		if FP >= .9972:
			return 'GOLD'
		elif FP >= .9939:
			return '&#149;GOLD'
		elif FP >= .9792:
			return 'NONE'
		elif FP >= .9725:
			return '&#149;IRON'
		else:
			return 'IRON'


	elif position == 'SS':
		if FP >= .9872:
			return 'GOLD'
		elif FP >= .9831:
			return '&#149;GOLD'
		elif FP >= .9653:
			return 'NONE'
		elif FP >= .9612:
			return '&#149;IRON'
		else:
			return 'IRON'

	return "NONE"
#end getFielding

# uses 2016 fielding cutoffs as a baseline
def getRange(position, RF, chances):
	if chances < 100 or position == 'DH': return "NONE"

	if position == '1B':
		if RF >= 9.0102:
			return 'GOLD'
		elif RF >= 8.7303:
			return 'GOLD&#149;'
		elif RF >= 6.6803:
			return 'NONE'
		elif RF >= 6.0000:
			return 'IRON&#149;'
		else:
			return 'IRON'


	elif position == '2B':
		if RF >= 4.7808:
			return 'GOLD'
		elif RF >= 4.4691:
			return 'GOLD&#149;'
		elif RF >= 3.8000:
			return 'NONE'
		elif RF >= 3.6186:
			return 'IRON&#149;'
		else:
			return 'IRON'

	elif position == '3B':
		if RF >= 2.8723:
			return 'GOLD'
		elif RF >= 2.6286:
			return 'GOLD&#149;'
		elif RF >= 2.1800:
			return 'NONE'
		elif RF >= 2.1190:
			return 'IRON&#149;'
		else:
			return 'IRON'


	elif position == 'C':
		if RF >= 8.8696:
			return 'GOLD'
		elif RF >= 8.3558:
			return 'GOLD&#149;'
		elif RF >= 6.8293:
			return 'NONE'
		elif RF >= 6.5484:
			return 'IRON&#149;'
		else:
			return 'IRON'


	elif position in ['OF','LF','RF','CF']:
		if RF >= 2.4231:
			return 'GOLD'
		elif RF >= 2.2143:
			return 'GOLD&#149;'
		elif RF >= 1.6618:
			return 'NONE'
		elif RF >= 1.5078:
			return 'IRON&#149;'
		else:
			return 'IRON'


	elif position == 'SS':
		if RF >= 4.4667:
			return 'GOLD'
		elif RF >= 4.3145:
			return 'GOLD&#149;'
		elif RF >= 3.6601:
			return 'NONE'
		elif RF >= 3.4516:
			return 'IRON&#149;'
		else:
			return 'IRON'

	return "NONE"
#end getRange
	
# name regex for removing things like (10-day DL, 40-man roster, etc)
regex = re.compile(r"\(.*?\)|\*|\#", re.IGNORECASE)


# specify the url
team_url = 'https://www.baseball-reference.com/teams/' + team_arg + '/' + year_arg + '.shtml'

# query the website and return the html to the variable 'html'
team_html = urllib2.urlopen(team_url)

# parse the html using beautiful soap and store in variable `soup`
soup = BeautifulSoup(team_html, 'html.parser')

team_name = soup.find('h1', attrs={'itemprop': 'name'}).find_all('span')


print '{0} {1}'.format(team_name[0].text.strip(), team_name[1].text.strip())

print '\nBatting'
print '------------'

valid_batting_stats = ['player', 'pos', 'AB', 'G', 'PA', 'batting_avg', 'onbase_perc', 'H', '2B', '3B', 'HR', 'SB', 'SO', 'SF', 'HBP', 'BB','award_summary'];
valid_fielding_stats = ['G', 'pos', 'chances', 'PO', 'A', 'E_def', 'Inn_def'];
valid_pitching_stats = ['player', 'IP', 'ER', 'G', 'GS', 'SO', 'BB', 'SV' ]

#firstName, lastName, position, throws, IP, ERA, G, GS, SO, BB, GIDP, SV, BA

# now get the batters
team_roster_soup = soup.find('table', attrs={'id': 'team_batting'}) \
.tbody.find_all('tr', class_=lambda x: x != 'thead')

# this starts on the team page
for batter_roster_row in team_roster_soup:
	# initialize the batter
	#batter = {}

	roster_batter_cells = batter_roster_row.find_all('td')
	
	roster_batter_info = {} #dictionary of batter info, mostly position and href
	for cell in roster_batter_cells:
		if cell['data-stat'] == 'pos': roster_batter_info['pos'] = cell.text
		if cell['data-stat'] == 'player': 
			roster_batter_info['href'] = cell.find('a')['href']

			name = cell.text.strip()

			if '*' in name:
				bats = 'L'
			elif '#' in name:
				bats = 'S'
			else:
				bats = 'R'

			name_parts = re.sub(regex, "", name).split(' ', 1)
	# end foreach cell in the row

	# non-batters are ignored
	if roster_batter_info['pos'] != 'P':
		processBatter = (debugPlayer == None) or (roster_batter_info['href'] == debugPlayer)
		
		if processBatter:
			player_html = urllib2.urlopen('https://www.baseball-reference.com' + roster_batter_info['href'])
			# with open('neil-walker.html', 'r') as myfile:
			# 	player_html=myfile.read()

			player_soup = BeautifulSoup(player_html, 'html.parser')

			##########################################
			#	Batting & Player Info
			##########################################

			# dictionary that represents the summed values of all the batter data
			batting_data = None
			career_AB = 0
			player_years = 0
			previous_year = None;

			# Get batting data and loop through the rows
			batting_rows = player_soup.find('table', attrs={'id': 'batting_standard'}).tbody.find_all(
				lambda tag: tag.name == 'tr' and 'hidden' not in tag.get('class', '')
			)
			
			for batting_row in batting_rows:
				batting_stats = {}
			 	batting_cells = batting_row.findChildren(['th', 'td']);

			 	current_year = batting_cells[0].text.strip()
			 	team = batting_cells[2].text.strip()

			 	# take all of the stat headers and values for this player and store them into a dictionary
		 		# represents one position for the given year
		 		for stat in batting_cells: 
		 			stat_category = stat['data-stat']

		 			if stat_category in valid_batting_stats:
		 				batting_stats[stat_category] = stat.text
				# end for each stat is batting cells

			 	if team != 'TOT' and batting_stats['AB']:
			 		career_AB = career_AB + int(batting_stats['AB']) 


			 		if current_year <> previous_year:
			 			previous_year = current_year
			 			player_years = player_years + 1

			 	# filter out rows that don't match the current year or are "TOT" rows
			 	if current_year == year_arg and team != 'TOT':
					if batting_data:
						batting_data['G'] = int(batting_data['G']) + int(batting_stats['G']) 
						batting_data['AB'] = int(batting_data['AB']) + int(batting_stats['AB']) 
						batting_data['PA'] = int(batting_data['PA']) + int(batting_stats['PA']) 
						batting_data['H'] = int(batting_data['H']) + int(batting_stats['H']) 
						batting_data['2B'] = int(batting_data['2B']) + int(batting_stats['2B']) 
						batting_data['3B'] = int(batting_data['3B']) + int(batting_stats['3B']) 
						batting_data['HR'] = int(batting_data['HR']) + int(batting_stats['HR']) 
						batting_data['SB'] = int(batting_data['SB']) + int(batting_stats['SB']) 
						batting_data['SO'] = int(batting_data['SO']) + int(batting_stats['SO']) 
						batting_data['SF'] = int(batting_data['SF']) + int(batting_stats['SF'])
						batting_data['HBP'] = int(batting_data['HBP']) + int(batting_stats['HBP'])
						batting_data['BB'] = int(batting_data['BB']) + int(batting_stats['BB'])
						batting_data['award_summary'] = batting_stats['award_summary']
					else:
						batting_data = batting_stats
				#end if
			# end for each
			 
			batting_data['career_AB'] = career_AB
			batting_data['player_years'] = player_years

			##########################################
			#	Fielding
			##########################################
			#
			# Fielding table for some reason is wrapped in a comment field which BeautifulSoup seems to ignore
			# We need to get the comment and then get the table from within the comment
			fielding_container = player_soup.find('div', attrs={'id': 'all_standard_fielding'})
			
			fieldRating = "NONE"
			rangeRating = "NONE"

			if fielding_container:
				comment = fielding_container.find(string=lambda text:isinstance(text,Comment))
				fielding_soup = BeautifulSoup(comment, 'html.parser')
				fielding_rows = fielding_soup.find('table').tbody.find_all('tr')
				
				# this dictionary represents all of the fielding values for each position for a single player
				fielding_data = {}
				for fielding_row in fielding_rows:
					fielding_cells = fielding_row.findChildren(['th', 'td'])
					
					current_year = fielding_cells[0].text.strip()
					team = fielding_cells[2].text.strip()

					# filter out rows that don't match the current year or are "TOT" rows
					if current_year == year_arg and team != 'TOT':
						position_fielding = {}

						# take all of the stat headers and values for this player and store them into a dictionary
						# represents one position for the given year
						for stat in fielding_cells: 
							stat_category = stat['data-stat']

							if stat_category in valid_fielding_stats:
								position_fielding[stat_category] = stat.text
						# end for each stat is fielding cells
						position = position_fielding['pos'];

						if position not in ['OF', 'P']:
							if position not in fielding_data:
								fielding_data[position] = position_fielding
							else:
								if position != 'DH':
									fielding_data[position]['G'] = int(fielding_data[position]['G']) + int(position_fielding['G'])
									fielding_data[position]['A'] = int(fielding_data[position]['A']) + int(position_fielding['A'])
									fielding_data[position]['PO'] = int(fielding_data[position]['PO']) + int(position_fielding['PO'])
									fielding_data[position]['chances'] = int(fielding_data[position]['chances']) + int(position_fielding['chances'])
									fielding_data[position]['E_def'] = int(fielding_data[position]['E_def']) + int(position_fielding['E_def'])
									fielding_data[position]['Inn_def'] = float(fielding_data[position]['Inn_def']) + float(position_fielding['Inn_def'])						
								# end if
							#end else
						#end if
					# end if current year and not total row
				# end for each fielding row

				# sort all positions played by appearances (G = Games Appeared)			
				sorted_fielding = sorted(fielding_data.items(), reverse=True, key=lambda x: int(x[1]['G']))

				position_string = ""
				i = 0	

				# get the top 3 positions by appearance
				while i < min(3, len(sorted_fielding)):
					processPosition = True
					position = sorted_fielding[i][0]
					gamesPlayed = int(sorted_fielding[i][1]['G'])
					
					# Don't include DH for players who played less than 15 games at that position
					if position == 'DH' and gamesPlayed < 20:
						processPosition = False

					# Only process positions with the right requirements
					if processPosition:
						if len(position_string): position_string += "/" + sorted_fielding[i][0]
						else: position_string = sorted_fielding[i][0]
					
					i += 1

				if sorted_fielding:
					# do some work to figure out RF and F%
					top_defense = sorted_fielding[0][1]
					if top_defense and top_defense['pos'] == 'DH':
						fieldRating = 'NONE'
						rangeRating = 'NONE'
					else:
						PO = int(top_defense['PO'])

						E = int(top_defense['E_def'])
						A = int(top_defense['A'])
						IP = float(top_defense['Inn_def'])
						G = int(top_defense['G'])
						chances = int(top_defense['chances'])

						FP = None
						RFG = None

						if float(PO + A + E) > 0:
							RFG = float(PO + A)/G
							FP = (PO + A)/float(PO + A + E)

						fieldRating = getFielding(sorted_fielding[0][0], FP, chances)
						rangeRating = getRange(sorted_fielding[0][0], RFG, chances)
					# end else
				# end if
			#end if

			H = int(batting_data['H'])
			BB = int(batting_data['BB'])
			SF = int(batting_data['SF'])
			HBP = int(batting_data['HBP'])
			AB = int(batting_data['AB'])

			PA = AB + BB + HBP + SF

			if AB > 0:
				BA = int(batting_data['H'])/float(AB)
			else:
				BA = 0.000
				
			if PA > 0:
				OBP = (H + BB + HBP) / float(PA)
			else:
				OBP = 0.000

			fieldingQualities = ""

			awards = batting_data['award_summary'].split(',')
			if 'GG' in awards:
				fieldingQualities = "GOLD / GOLD"
			elif fieldRating and rangeRating:
				if fieldRating <> "NONE" and rangeRating <> "NONE":
					fieldingQualities = fieldRating + " / " + rangeRating		
			else:
				fieldingQualities = "NONE / NONE"

			##########################################
			#	Final output
			##########################################
			# 			
			# firstName, lastName, position, bats, AB, PA, BA, OBP, H, 2B, 3B, HR, SB, SO, FLD, RNG
			print '{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}'.format(
					name_parts[0].encode('utf-8').strip(),
					name_parts[1].encode('utf-8').strip(),
					position_string,
					bats,
					batting_data['G'],
					batting_data['AB'],
					BA,
					batting_data['H'],
					batting_data['2B'],
					batting_data['3B'],
					batting_data['HR'],
					batting_data['BB'],
					batting_data['SO'],
					batting_data['SB'],
					batting_data['player_years'],
					fieldingQualities
				)
		# end if player is Hernan Perez (temporary)
	# end if pos not 'P'
#end for each batter

print '\n\nPitching'
print '------------'

##########################################
#	Pitching & Player Info
##########################################

#firstName, lastName, position, throws, IP, ERA, G, GS, SO, BB, GIDP, SV, BA

# now get the pitchers
team_roster_soup = soup.find('table', attrs={'id': 'team_pitching'}) \
.tbody.find_all('tr', class_=lambda x: x != 'thead')

# this starts on the team page
for pitcher_roster_row in team_roster_soup:
	# initialize the pitcher
	pitcher = {}

	roster_pitcher_cells = pitcher_roster_row.find_all('td')
	#print roster_pitcher_cells[1].get('data-stat')

	roster_pitcher_info = {} #dictionary of pitcher info, mostly position and href
	for cell in roster_pitcher_cells:
		if cell['data-stat'] == 'pos': roster_pitcher_info['pos'] = cell.text
		if cell['data-stat'] == 'player': 
			roster_pitcher_info['href'] = cell.find('a')['href']
			name = cell.text.strip()

			if "*" in name:
				throws = 'L'
			else:
				throws = 'R'

			name_parts = re.sub(regex, "", name).split(' ', 1)

	# end foreach cell in the row

	#temporary to save on calls
	processPitcher = (debugPlayer == None) or (roster_pitcher_info['href'] == debugPlayer)

	if processPitcher:
		player_html = urllib2.urlopen('https://www.baseball-reference.com' + roster_pitcher_info['href'])
		
		# create soup for the pitcher's page we're getting stats from
		player_soup = BeautifulSoup(player_html, 'html.parser')

		# dictionary that represents the summed values of all the pitcher data
		pitching_data = None
		player_years = 0
		previous_year = None;


		# Get pitching data and loop through the rows
		# We need to check if the table exists though.  Some batters have also pitched and don't have a standard pitching table
		pitching_table = player_soup.find('table', attrs={'id': 'pitching_standard'})
		if pitching_table:
			pitching_rows = pitching_table.tbody.find_all(
				lambda tag: tag.name == 'tr' and 'hidden' not in tag.get('class', '')
			)	

			for pitching_row in pitching_rows:
				pitching_stats = {}
			 	pitching_cells = pitching_row.findChildren(['th', 'td']);

			 	current_year = pitching_cells[0].text.strip()
			 	team = pitching_cells[2].text.strip()

			 	# take all of the stat headers and values for this player and store them into a dictionary
		 		# represents one position for the given year
		 		for stat in pitching_cells: 
		 			stat_category = stat['data-stat']

		 			if stat_category in valid_pitching_stats:
		 				pitching_stats[stat_category] = stat.text
				# end for each stat is pitching cells

				if current_year <> previous_year and team != 'TOT' and pitching_stats['IP']:
		 			previous_year = current_year
		 			player_years = player_years + 1

			 	# filter out rows that don't match the current year or are "TOT" rows
			 	if current_year == year_arg and team != 'TOT':					 					
					if pitching_data:
						pitching_data['GS'] = int(pitching_data['GS']) + int(pitching_stats['GS']) 
						pitching_data['IP'] = float(pitching_data['IP']) + float(pitching_stats['IP']) 
						pitching_data['ER'] = int(pitching_data['ER']) + int(pitching_stats['ER']) 
						pitching_data['G'] = int(pitching_data['G']) + int(pitching_stats['G']) 
						pitching_data['GS'] = int(pitching_data['GS']) + int(pitching_stats['GS']) 
						pitching_data['SO'] = int(pitching_data['SO']) + int(pitching_stats['SO']) 
						pitching_data['BB'] = int(pitching_data['BB']) + int(pitching_stats['BB']) 
						pitching_data['SV'] = int(pitching_data['SV']) + int(pitching_stats['SV']) 
					else:
						pitching_data = pitching_stats
				#end if
			# end for each
			 
			pitching_data['player_years'] = player_years
			
			IP = float(pitching_data['IP'])
			if IP > 0:
				ERA = 9 * (int(pitching_data['ER']) / IP)

				# first, last, bats, throws, IP, G, GS, SV, BB, SO, ERA, Seasons
				print '{},{},{},{},{},{},{},{},{},{},{},{}'.format(
							name_parts[0].encode('utf-8').strip(),
							name_parts[1].encode('utf-8').strip(),
							throws,
							throws,
							pitching_data['IP'],
							pitching_data['G'],
							pitching_data['GS'],
							pitching_data['SV'],
							pitching_data['BB'],
							pitching_data['SO'],
							ERA,
							pitching_data['player_years']
						)

		