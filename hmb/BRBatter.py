

class BRBatter:
    FirstName = ""
    LastName = ""
    PlayerId = ""
    Position = ""
    G = 0
    PA = 0
    AB = 0
    Doubles = 0
    Triples = 0
    HR = 0
    SB = 0
    BB = 0
    SO = 0
    BA = .000
    HBP = 0
    Years = 0
    RtotYr = 0

    #G	PA	AB	2B	3B	HR	SB	BB	SO	BA	HBP
    def getHmbStatLine(self):
        return (
         f"{self.Position},{self.FirstName} {self.LastName}\\{self.PlayerId},{self.G},{self.PA},{self.AB},"
         f"{self.Doubles},{self.Triples},{self.HR},{self.SB},{self.BB},{self.SO},{self.BA},{self.HBP}"
        )