class BRFielder:
    FirstName = ""
    LastName = ""
    G = 0
    CH = 0
    FLP = 0.0
    RTotYr = 0
    Position = ""
    PlayerId = ""

    def getHmbStatLine(self):
     return (
         f"{self.FirstName} {self.LastName}\\{self.PlayerId},{self.G},{self.CH},{self.FLP},{self.RTotYr}"
     )