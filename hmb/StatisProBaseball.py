# import libraries
import re
import operator
import sys
import os
import pprint

# TODO: Get current year's roster but last year's stats
# TODO: Command line switches to kill cache
class StatisProBaseball:
    
    def __init__(self):
        # name regex for removing things like (10-day DL, 40-man roster, etc)
        pass

    def RateFielding(self, player):
        # # Display Hernan Perez data for testing
        # # Now need to figure out E (F%), T (Ast), and CD (RTOTyr)
        # # modify url to be dynamic
        # # spit out the values into notepad
        # player = fielders['bellira01']
        positions = player['positions']
        appearances = player['appearances']

        # Position data on the player is unsorted, so sort the positions played by number of games most to least
        appearances = sorted(appearances.items(), key=operator.itemgetter(1), reverse=True)

        ratings = []
        # loop through the sorted appearances and do stuff with the data for that position
        for posValues in appearances:
            pos,games = posValues
            
            # pos is the current position
            # positions[pos] is the data for that position
            data = positions[pos]

            # A, G, fld, rtot_yr
            # Format: position-appearances(error) arm:
            # c121 E5 TA
            # lf4 E0 T3
            # cf13 E0 T3
            # rf41 E3 T3
            # dh-6					
            
            if pos=='DH':
                rating_string = f"{pos}-{data['G']}"
            else:
                rating_string = f"{pos}-{data['G']}(E?)"
            
            ratings.append(rating_string)

        return ratings
					
					
					
