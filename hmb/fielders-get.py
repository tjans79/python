# import libraries
import BRTeamEngine
import sys
import spinner
import subprocess
import HMBRater
from pprint import pprint
import json
import os

clear = lambda: subprocess.call('cls||clear', shell=True)
clear()

# get team and year arguments
if len(sys.argv) == 3:
	year_arg = sys.argv[1] #"2017"
	team_arg = sys.argv[2]
else:
	year_arg = '1994'
	team_arg = 'ATL'

# https://www.baseball-reference.com/leagues/NL/2017.shtml

# make it so you can potentially pass a list of teams and player names or find a way to let them choose the player
# perhaps even better, store it in sqlite and query it from there, adding players to a shopping cart.
# also need a way to bust cache if you want an in-season team

bre = BRTeamEngine.BRTeamEngine(team_arg, year_arg)

# appearances = bre.getAdvancedFielder("https://www.baseball-reference.com/players/m/moustmi01.shtml", "2019")
# print(appearances[0])

fielders = bre.getFielders2()

# # #clear the file
# f = open("output.txt", "w")
# f.write(bre.team_full_name + "\n\n")
# f.write("Batters\n")
# f.write("--------------\n")
# for batter in batters:
# 	f.write(batter.getHmbStatLine())
# 	f.write("\n")

# f.write("\n")
# f.close()

# outputPath = os.path.abspath("output.txt")
# subprocess.call(['cmd.exe', '/c', outputPath])