# import libraries
import time
import urllib.request
import urllib.parse
import re
from bs4 import BeautifulSoup
from bs4 import Comment
import operator
import sys
import BRBatter, BRPitcher, BRServiceTime, BRFielder
import os

# TODO: Get current year's roster but last year's stats
# TODO: Command line switches to kill cache
class FGTeamEngine:
    valid_batting_stats = ['player', 'pos', 'AB', 'G', 'PA', 'batting_avg', 'onbase_perc', 'H', '2B', '3B', 'HR', 'SB', 'SO', 'SF', 'HBP', 'BB','award_summary']
    valid_fielding_stats = ['G', 'pos', 'chances', 'PO', 'A', 'E_def', 'Inn_def']
    valid_pitching_stats = ['player', 'IP', 'ER', 'G', 'GS', 'SO', 'BB', 'SV' ]
    regex = re.compile(r"\(.*?\)|\*|\#", re.IGNORECASE)
    soup = None
    debugPlayer = None
    #debugPlayer = '/players/y/yelicch01.shtml'
    year = ""
    team = ""
    city = ""
    slugify_strip_re = re.compile(r'[^\w\s-]')
    slugify_hyphenate_re = re.compile(r'[-\s]+')

    def __init__(self, team, year):
        # name regex for removing things like (10-day DL, 40-man roster, etc)
        self.team = team
        self.year = year
        self.team_full_name = ""
        self.city = ""

        #TeamId
        #URL: 'https://www.fangraphs.com/projections.aspx?pos=all&stats={bat|pitch}&type=zips&team={teamid}&lg=all&players=0'

        # specify the url
        team_url = 'https://www.fangraphs.com/projections.aspx?pos=all&stats=bat&type=zips&team=23&lg=all&players=0&sort=3,d'
        response = self.getOrCached(team_url)
        team_html = response['result']

        # parse the html using beautiful soap and store in variable `soup`
        self.soup = BeautifulSoup(team_html, 'html.parser')

        #team_name = self.soup.find('h1', attrs={'itemprop': 'name'}).find_all('span')

        self.city = "Milwaukee"
        self.team_full_name = "Milwaukee Projections 2020"
        #self.city = team_name[1].text.rsplit(' ', 1)[0]
        #self.team_full_name = '{0} {1}'.format(team_name[0].text.strip(), team_name[1].text.strip())

    def getOrCached(self, url):
        fileName = 'cache/' + self.slugify(url)
        
        if not os.path.exists('cache'):
            os.makedirs('cache')

        response = {}

        try:
            fh = open(fileName, 'r')
            print("from cache")
            response['result'] = fh.read()
            response['was_cached'] = True
        except FileNotFoundError:
            print("from website")
            time.sleep(1) # pause so we don't kill the server

            # query the website and return the html to the variable 'html'
            httpResponse = urllib.request.urlopen(url)
            responseBody = str(httpResponse.read())
            fh = open(fileName, 'w')
            fh.write(responseBody)

            response['result'] = responseBody
            response['was_cached'] = False
        return response

    def slugify(self, value):
        value = value.replace('/','_') \
            .replace(':','_') \
            .replace('.','_') \
            .replace('=','_') \
            .replace('?','_')
        return value

    def getBatters(self):
        batters = []
        service_times = []
        batterCount = 0

        team_roster_soup = self.soup.find('table', attrs={'class': 'rgMasterTable'})
        player_soup = team_roster_soup.tbody.find_all('tr')
        header_soup = team_roster_soup.thead.find_all('tr')

        # Build column indices in case they are out of order on different teams
        column = 0
        c = {}   
        for header_row in header_soup:
            cells = header_row.find_all('th')
            for cell in cells:
                c[cell.text] = column
                column += 1

        for player_row in player_soup:
            cells = player_row.find_all('td')
            
            name = cells[c['Name']].text
            name_parts = name.split(' ',1)

            link = cells[c['Name']].find('a')
            qs = dict(urllib.parse.parse_qsl(urllib.parse.urlsplit(link['href']).query))

            # Get handedness and service time
            response = self.getOrCached('https://www.fangraphs.com/' + link['href'])
            profile_html = response['result']
            profile_soup = BeautifulSoup(profile_html, 'html.parser')  
            info_boxes = profile_soup.find_all('div', attrs={'class': 'player-info-box-item'})
            
            hand = ""
            for ib in info_boxes:
                if ib.text.startswith("Bats/Throws: "):
                    batsThrows = (ib.text.replace("Bats/Throws: ", "")).split("/")
                    if batsThrows[0] == 'L':
                        hand = "*"
                    elif batsThrows[0] == 'B':
                        hand = "#"
            
            service_years = 0
            rr_details = profile_soup.find_all('tr', attrs={'class': 'player-info__bio-rr-details'})
            if len(rr_details):
                tds = rr_details[0].findChildren(['td'])
                service_years = int(round(float(tds[0].text), 0))
            
            fld = float(cells[c['Fld']].text)
            rtotyr = 0

            # convert FG's Fld rating into appropriate rating for HMB
            if fld >= 10:
                rtotyr = 15
            elif fld >= 5:
                rtotyr = 14
            elif fld >= 0:
                rtotyr = 7
            elif fld >=-10:
                rtotyr = -8
            else:
                rtotyr = -15

            ##########################################
            #	Final output
            ##########################################
            # 			
            # G	PA	AB	2B	3B	HR	SB	BB	SO	BA	HBP
            batter = BRBatter.BRBatter()
            batter.FirstName = name_parts[0].strip()
            batter.LastName = name_parts[1].strip() + hand
            batter.Position = qs['position']
            batter.PlayerId = qs['playerid']
            batter.G = int(cells[c['G']].text)
            batter.PA = int(cells[c['PA']].text)
            batter.AB = int(cells[c['AB']].text)
            batter.Doubles = int(cells[c['2B']].text)
            batter.Triples = int(cells[c['3B']].text)
            batter.HR = int(cells[c['HR']].text)
            batter.SB = int(cells[c['SB']].text)
            batter.BB = int(cells[c['BB']].text)
            batter.SO = int(cells[c['SO']].text)
            batter.Years = service_years
            batter.RtotYr = rtotyr

            if cells[c['AVG']].text:
                batter.BA = float(cells[c['AVG']].text)
            else:
                batter.BA = .000  

            batter.HBP = int(cells[c['HBP']].text)

            batters.append(batter)           

            batterCount += 1

            if(batterCount == 20):
                return batters

        return batters    
    # end getBatters

    def getPitchers(self):
        # specify the url
        team_url = 'https://www.fangraphs.com/projections.aspx?pos=all&stats=pit&type=zips&team=23&lg=all&players=0&sort=7,d'
        response = self.getOrCached(team_url)
        team_html = response['result']

        # parse the html using beautiful soap and store in variable `soup`
        pitcher_soup = BeautifulSoup(team_html, 'html.parser')

        pitchers = []

        team_roster_soup = pitcher_soup.find('table', attrs={'class': 'rgMasterTable'})
        player_soup = team_roster_soup.tbody.find_all('tr')
        header_soup = team_roster_soup.thead.find_all('tr')

        # Build column indices in case they are out of order on different teams
        column = 0
        c = {}   
        for header_row in header_soup:
            cells = header_row.find_all('th')
            for cell in cells:
                c[cell.text] = column
                column += 1
        
        for player_row in player_soup:
            cells = player_row.find_all('td')
            
            name = cells[c['Name']].text
            name_parts = name.split(' ',1)

            link = cells[c['Name']].find('a')
            qs = dict(urllib.parse.parse_qsl(urllib.parse.urlsplit(link['href']).query))
    
            # Get handedness and service time
            response = self.getOrCached('https://www.fangraphs.com/' + link['href'])
            profile_html = response['result']
            profile_soup = BeautifulSoup(profile_html, 'html.parser')  
            info_boxes = profile_soup.find_all('div', attrs={'class': 'player-info-box-item'})
            
            hand = ""
            for ib in info_boxes:
                if ib.text.startswith("Bats/Throws: "):
                    batsThrows = (ib.text.replace("Bats/Throws: ", "")).split("/")
                    if batsThrows[0] == 'L':
                        hand = "*"
                    elif batsThrows[0] == 'B':
                        hand = "#"
            
            service_years = 0
            rr_details = profile_soup.find_all('tr', attrs={'class': 'player-info__bio-rr-details'})
            if len(rr_details):
                tds = rr_details[0].findChildren(['td'])
                service_years = int(round(float(tds[0].text), 0))

            ##########################################
            #	Final output
            ##########################################
            # 			
            # Pos, Name, ERA, G, GS, IP, BB, SO
            pitcher = BRPitcher.BRPitcher()
            pitcher.FirstName = name_parts[0].strip()
            pitcher.LastName = name_parts[1].strip() + hand
            pitcher.FirstName = name_parts[0].strip()
            pitcher.LastName = name_parts[1].strip()
            pitcher.Position = qs['position']
            pitcher.PlayerId = qs['playerid']
            
            pitcher.ERA = float(cells[c['ERA']].text)
            pitcher.G = int(cells[c['G']].text)
            pitcher.GS = int(cells[c['GS']].text)
            pitcher.IP = round(float(cells[c['IP']].text),0)
            pitcher.BB = int(cells[c['BB']].text)
            pitcher.SO = int(cells[c['SO']].text)
            pitcher.Years = service_years
            pitchers.append(pitcher)

        return pitchers   
    # end getPitchers

    def getServiceTime(self):
        serviceTimes = []
        
        service_time_soup = None

        # for some reason this table is wrapped in a comment.  Need to find the comment it's in and bust it out
        for comment in self.soup.findAll(text=lambda text:isinstance(text, Comment)):
            if "id=\"appearances\"" in comment:
                comment_soup = BeautifulSoup(comment, 'html.parser')
                service_time_soup = comment_soup.find('table', attrs={'id': 'appearances'}) \
                .tbody.find_all('tr', class_=lambda x: x != 'thead') 
                break

        if service_time_soup:
            # this starts on the team page
            for servicetime_roster_row in service_time_soup:
                roster_servicetime_cells = servicetime_roster_row.find_all(['td','th'])

                roster_servicetime_info = {} #dictionary of servicetime info, mostly position and href
                for cell in roster_servicetime_cells:
                    if cell['data-stat'] == 'player': 
                        name = cell.text.strip()
                        name_parts = name.split(' ', 1) #re.sub(self.regex, "", name).split(' ', 1) 
                        playerUrl = cell.find('a')['href'] 
                        slashIndex = playerUrl.rindex("/") + 1
                        dotIndex = playerUrl.rindex(".")
                        playerId = playerUrl[slashIndex:dotIndex]
                    else:
                        roster_servicetime_info[cell['data-stat']] = cell.text
                # end foreach cell in the row

                processServiceTime = True

                if processServiceTime:

                    ##########################################
                    #	Final output
                    ##########################################
                    # 			
                    serviceTime = BRServiceTime.BRServiceTime()
                    serviceTime.FirstName = name_parts[0].strip()
                    serviceTime.LastName = name_parts[1].strip()
                    serviceTime.PlayerId = playerId
                    serviceTime.Years = roster_servicetime_info['experience']
                    serviceTimes.append(serviceTime)
                else:
                    pass

        return serviceTimes   
    # end getServiceTime

    def getAdvancedFielder(self, url, year):
        print("Getting advanced fielding: " + url)
        # Thames, Moustakas, Cain, Braun, Shaw, Gamel, Perez, Gamel
        # Utility: https://www.baseball-reference.com/players/p/perezhe01.shtml
        # OF: https://www.baseball-reference.com/players/g/gamelbe01.shtml
        # 1B/RF/DH: https://www.baseball-reference.com/players/t/thameer01.shtml
        # 3B/2B/DH: https://www.baseball-reference.com/players/m/moustmi01.shtml
        # LF/RF: https://www.baseball-reference.com/players/y/yelicch01.shtml
        # MI, CI, OF... 

        playerHtml = self.getOrCached(url)
        
        # parse the html using beautiful soap and store in variable `soup`
        playerSoup = BeautifulSoup(playerHtml, 'html.parser')

        fielderSoup = ""
        for comment in playerSoup.findAll(text=lambda text:isinstance(text, Comment)):
            if "id=\"standard_fielding\"" in comment:
                commentSoup = BeautifulSoup(comment, 'html.parser')
                fielderSoup = commentSoup.find('table', attrs={'id': 'standard_fielding'}) \
                .tbody.find_all('tr', class_=lambda x: x != 'thead') 
                break
        
        if fielderSoup:
            appearances = []
            for row in fielderSoup:
                fieldingCells = row.find_all(['td','th'])

                appearanceRow = {}

                for cell in fieldingCells:
                    currentYear = cell.text.strip()
                    if cell['data-stat'] == 'year_ID':
                        
                        # skip unknown years
                        if currentYear != year:
                            continue #stop processing this year
                                           
                        appearanceRow['year'] = currentYear

                    else:
                        appearanceRow[cell['data-stat']] = cell.text                                               
                # end for

                appearances.append(appearanceRow)
            #end for

            # now we have appearances.  We need to loop through each row, grabbing the top 3 positions ordered by appearances
            # grab the RToT/Yr for each position, use the value from the most appearances
            # check award_summary for GG
            # perhaps run fielding through the 2016 data (or write a script to update it) by position to get best value
            # at minimum, grab top 3 positions and return as a position string
            # remember to consolidate players that played on multiple teams (or potentially just use the team we're processing)
            return appearances
        #end if


    ################################################
    #  GetFielders
    ################################################
    def getFielders(self):
        fielders = []
        
        fielder_soup = None

        # for some reason this table is wrapped in a comment.  Need to find the comment it's in and bust it out
        for comment in self.soup.findAll(text=lambda text:isinstance(text, Comment)):
            if "id=\"standard_fielding\"" in comment:
                comment_soup = BeautifulSoup(comment, 'html.parser')
                fielder_soup = comment_soup.find('table', attrs={'id': 'standard_fielding'}) \
                .tbody.find_all('tr', class_=lambda x: x != 'thead') 
                break

        if fielder_soup:
            # this starts on the team page
            for fielding_roster_row in fielder_soup:
                roster_fielding_cells = fielding_roster_row.find_all(['td','th'])

                roster_fielding_info = {} #dictionary of servicetime info, mostly position and href
                for cell in roster_fielding_cells:
                    if cell['data-stat'] == 'player': 
                        name = cell.text.strip()
                        name_parts = name.split(' ', 1) #re.sub(self.regex, "", name).split(' ', 1) 
                        playerUrl = cell.find('a')['href'] 
                        slashIndex = playerUrl.rindex("/") + 1
                        dotIndex = playerUrl.rindex(".")
                        playerId = playerUrl[slashIndex:dotIndex]
                    else:
                        roster_fielding_info[cell['data-stat']] = cell.text
                # end foreach cell in the row

                processFielding = True

                if processFielding:

                    ##########################################
                    #	Final output
                    ##########################################
                    # 			
                    fielder = BRFielder.BRFielder()
                    fielder.FirstName = name_parts[0].strip()
                    fielder.LastName = name_parts[1].strip()
                    fielder.PlayerId = playerId
                    fielder.G = roster_fielding_info['G']
                    fielder.CH = roster_fielding_info['chances']
                    fielder.FLP = roster_fielding_info['fielding_perc']
                    fielder.RTotYr = roster_fielding_info['tz_runs_total_per_season']
                    fielders.append(fielder)
                else:
                    pass

        return fielders   
    # end getServiceTime